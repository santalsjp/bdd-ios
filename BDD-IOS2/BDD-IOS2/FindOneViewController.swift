//
//  FindOneViewController.swift
//  BDD-IOS2
//
//  Created by Santiago Pazmiño on 2/2/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit

class FindOneViewController: UIViewController {

    @IBOutlet weak var adressLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    
    var persona:Person?
    private let manegaedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = persona?.name
        
        adressLabel.text = persona?.adress!
        phoneLabel.text = persona?.phone!
        // Do any additional setup after loading the view.
    }
    @IBAction func button(_ sender: Any) {
        manegaedObjectContext.delete(persona!)
        
        do {
            try manegaedObjectContext.save()
            navigationController?.popViewController(animated: true)
        } catch  {
            print("error al eliminar")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
