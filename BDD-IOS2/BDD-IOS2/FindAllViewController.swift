//
//  FindAllViewController.swift
//  BDD-IOS2
//
//  Created by Santiago Pazmiño on 2/2/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit
import CoreData

class FindAllViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var personTableView: UITableView!
    
    private let manegaedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var personaArray:[Person] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchAll()
        
    }
    
    func listPersonas(_ persona: [Person]) {
        personaArray = persona
        personTableView.reloadData()
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return personaArray.count
        default:
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personaCell") as! PersonaTableViewCell
        cell.fillData(personaArray[indexPath.row])
        //     cell.textLabel?.text = pokemonArray[indexPath.row].pkName
        return cell
    }
    
    func fetchAll(){
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            let results = try manegaedObjectContext.fetch(request)
            personaArray = results
            for p in results {
                let person = p as! Person
                print("Nombre: \(person.name!) , ",terminator:"")
                print("Direccion: \(person.adress!) , ",terminator:"")
                print("Telefono: \(person.phone!)  ",terminator:"")
                print()
                print()
            }
        } catch  {
            
        }
    }
    
    
    

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
        

    
    


}
