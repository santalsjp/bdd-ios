//
//  ViewController.swift
//  BDD-IOS2
//
//  Created by Santiago Pazmiño on 29/1/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var adressTextField: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    var persona:Person?
    
    private let manegaedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func saveButton(_ sender: Any) {
        
        let entyDescription = NSEntityDescription.entity(forEntityName: "Person", in: manegaedObjectContext)
        let person = Person(entity: entyDescription!, insertInto: manegaedObjectContext)
        
        person.name = nameTextField.text!
        person.adress = adressTextField.text!
        person.phone = phoneTextField.text!
        
        do {
            try manegaedObjectContext.save()
            clearFields()
        } catch  {
            print("Error al guardar")
            
        }
    }
    
    func clearFields (){
        nameTextField.text = ""
        adressTextField.text = ""
        phoneTextField.text = ""
    }
    
    
    @IBAction func findButton(_ sender: Any) {
        
        if nameTextField.text! == ""
        {
            performSegue(withIdentifier: "FindAll", sender: self)
            return
        }
        
        fetchOne()
        
        
        
        }
    
    func fetchOne(){
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        let pred = NSPredicate(format: "name = %@", nameTextField.text!)
        
        request.predicate = pred
        
        do {
            let results = try manegaedObjectContext.fetch(request)
            if results.count > 0 {
                let person = results[0]
               // adressTextField.text = person.adress!
               // phoneTextField.text = person.phone!
                persona = person
                
                
                
            }
            else
            {
            clearFields()
            let alertController = UIAlertController(title: "Error!!!", message: "No se a encontrado el elemento buscado", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            }
        } catch let error {
            
            print(error)
        }
        
        
    }
        
    func fetchAll(){
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            let results = try manegaedObjectContext.fetch(request)
            
            for p in results {
                let person = p as! Person
                print("Nombre: \(person.name!) , ",terminator:"")
                print("Direccion: \(person.adress!) , ",terminator:"")
                print("Telefono: \(person.phone!)  ",terminator:"")
                print()
                print()
            }
        } catch  {
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "FindOneSegue"){
            let destination = segue.destination as! FindOneViewController
            destination.persona = persona
        }
    }
    
}


