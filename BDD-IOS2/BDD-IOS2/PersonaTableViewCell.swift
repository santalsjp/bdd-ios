//
//  PersonaTableViewCell.swift
//  BDD-IOS2
//
//  Created by Santiago Pazmiño on 2/2/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit

class PersonaTableViewCell: UITableViewCell {

    @IBOutlet weak var nombreLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func fillData(_ persona:Person){
        nombreLabel.text = persona.name ?? "ND"
        
    }

}
